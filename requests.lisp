(in-package :dx-starter-site)

(defclass dx-site-acceptor (site-acceptor)
  ((login-url :initarg :login-url
              :initform nil
              :accessor login-url)
   (data-path :initarg :data-path
              :initform nil
              :accessor data-path)
   (permissions :initarg :permissions
                :initform nil
                :accessor permissions)))

(defun acceptor* ()
  (hunchentoot:request-acceptor *request*))

(defun check-permission (uri &optional sub-permission)
  (let ((user (current-user)))
    (cond ((not user)
           nil)
          ((super-user-p user)
           t)
          (t
           (let ((permission (find uri (permissions user) :key #'car
                                                          :test #'equal)))
             (and permission
                  (or (not sub-permission)
                      (member sub-permission (cdr permission)
                              :test #'equal))
                  t))))))

(defun check-permission-or-error (uri &optional sub-permission)
  (or (check-permission uri sub-permission)
      (signal 'permission-denied)))

(defun check-page-permission (&optional sub-permission)
  (check-permission (script-name*) sub-permission))

(defun check-page-permission-or-error (&optional sub-permission)
  (check-permission-or-error (script-name*) sub-permission))

(defun add-permission (acceptor name permissions)
  (check-type name string)
  (setf (alexandria:assoc-value (permissions acceptor) name :test #'equal)
        permissions))

(defmacro define-dx-handler (acceptor description lambda-list &body body)
  (when (atom description)
    (setf description (list description)))
  (destructuring-bind (name &rest args
                       &key uri for-everyone permissions
                       &allow-other-keys) description
    `(progn
       ,(when (and uri (not for-everyone))
          `(add-permission ,acceptor ,uri ',permissions))
       (define-easy-handler (,name ,@args :allow-other-keys t) ,lambda-list
           ,@(when (and uri (not for-everyone))
               `((check-permission-or-error ,uri)))
         ,@body))))

(defgeneric render-error-page (acceptor &key))
(defgeneric render-permission-denied-page (acceptor &key))
(defgeneric login-not-required (acceptor script-name))

(defmethod render-error-page ((acceptor site-acceptor) &key condition)
  (with-html-string
    (:html (:head (:title (esc (frmt "Error - ~a" (script-name*)))))
           (:body
            (:div :class "error-description"
                  (:strong :style "color: red;"
                           "Error: ")
                  (esc (princ-to-string condition)))))))

(defmethod render-permission-denied-page ((acceptor site-acceptor) &key)
  (let ((title (frmt "Access denied - ~a" (script-name*))))
   (with-html-string
     (:html (:head (:title (esc title)))
            (:body
             (:div :class "error-description"
                   (:strong :style "color: red;"
                            "Error: ")
                   (esc title)))))))

(defmethod login-not-required ((acceptor site-acceptor) script-name)
  (equal script-name (login-url acceptor)))

(defmethod hunchentoot:maybe-invoke-debugger ((condition error))
  (if (debug-errors-p (acceptor*))
      (invoke-debugger condition)
      (throw 'error condition)))

(defun call-with-error-handling (function)
  (if (debug-errors-p (acceptor*))
      (funcall function)
      (let ((condition
              (catch 'error
                (funcall function))))
        (if (typep condition 'error)
            (render-error-page (acceptor*) :condition condition)
            condition))))

(defmacro with-error-handling (&body body)
  `(call-with-error-handling
    (lambda () ,@body)))

(define-condition permission-denied ()
  ())

(defun call-with-permissions (function)
  (handler-case (funcall function)
    (permission-denied ()
      (render-permission-denied-page (acceptor*)))))

(defmacro with-permissions (&body body)
  `(call-with-permissions
    (lambda () ,@body)))

(defmethod handle-request :before ((acceptor site-acceptor) request)
  (unless (equal (script-name*) (login-url acceptor))
    (unless (equal (session-value 'current-uri) (request-uri*))
      (setf (session-value 'previous-uri) (session-value 'current-uri)
            (session-value 'previous-page) (session-value 'current-page)))
    (setf (session-value 'current-uri) (request-uri*)
          (session-value 'current-page) (script-name*))))

(defun redirect-ajax-to-login ()
  (json:encode-json-to-string
   '(nil
     ("window.location='~A';" (login-url (acceptor*))))))

(defmethod handle-request :around ((acceptor site-acceptor) request)
  (let ((script-name (script-name request)))
    (cond ((or (current-user)
               (login-not-required acceptor script-name))
           (let (*print-pretty*
                 (wfx::*widget-parameters* nil))
             (with-error-handling
               (with-permissions
                 (call-next-method)))))
          ((alexandria:starts-with-subseq (format nil "~Aajax/" (site-url acceptor))
                                          script-name)
           (redirect-ajax-to-login))
          (t
           (setf (session-value 'redirect-after-login) script-name)
           (redirect (login-url acceptor))))))
