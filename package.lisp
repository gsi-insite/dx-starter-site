(defpackage :dx-starter-site
  (:use :cl :hunchentoot :cl-who :wfx :xdb2 :dx-utils)
  (:shadowing-import-from :xdb2 :id)
  (:export
   ;;common
   #:current-user
   #:super-user-p

   ;;requests
   #:dx-site-acceptor
   #:login-url
   #:data-path
   #:permissions

   #:check-permission
   #:define-dx-handler))

