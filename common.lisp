(in-package :dx-starter-site)

;;(defvar *widget-parameters*)

(defun current-user ()
  (when (boundp '*session*)
    (session-value 'user)))

(defun (setf current-user) (user)
  (when (boundp '*session*)
    (setf (session-value 'user) user)))

(defgeneric super-user-p (user))

;;;

(defun render-select (name items value &key first-value blank-allowed)
  (let ((select (make-widget 'select
                             :name name)))
    (setf (blank-allowed select) blank-allowed)
    (setf (first-item select) first-value)
    (setf (items select) items)
    (when value
     (setf (value select) value))
    (render select)
    select))

(defun render-edit-field (name value
                          &key data-element required 
                          (type "text") 
                          data (blank-allowed t)
                          min
                          max
                          width)

  (with-html
    (case type
      (:span
       (htm (:input
             :style (if width
                        (format nil "width:~A;" width)
                        (format nil "width:~A;" "300px"))
             :type "text"
             :name name
             :disabled t
             :readonly t
             :value (escape value))))
      (:select
       (render-select (or data-element name) data value
                      :blank-allowed blank-allowed))
      (:textarea
       (htm (:textarea
             :style (if width
                        (format nil "width:~A;" width)
                        (format nil "width:~A;" "300px"))
             :class (if required "required")
             :name name
             :cols 85 :rows 5
             (str (escape value)))))
      (:password
       (htm (:input
             :style (if width
                        (format nil "width:~A;" width)
                        (format nil "width:~A;" "300px"))
             :type "password"
             :class (if required "required")
             :name name
             :value (escape value))))
      (:date
       (htm (:input :type "text"
                    :style (if width
                               (format nil "width:~A;" width)
                               (format nil "width:~A;" "300px"))
                    :class "datepicker"
                    :name name
                    :value (escape value)))
       (defer-js "$('.datepicker').datepicker({dateFormat: 'dd M yy'})"))
      (:time
       (htm (:input :type "text"
                    :style (if width
                               (format nil "width:~A;" width)
                               (format nil "width:~A;" "100px"))
                    :class (if required "required")
                    :name name
                    :pattern  "(\d{2}([\\:]\d{2})?)"
                    :value (escape value)
                    (if required "required")))
       )
      (t
       (htm
        (:div (:input :type type
                      :style (if width
                                 (format nil "width:~A;" width)
                                 (format nil "width:~A;" "300px"))
                      :class (if required "required")
                      :name name
                      :min min
                      :max max
                      :value (escape value)))
        (:div :style "display:none;"
              :name (format nil "validate-~A" name)
              :id (format nil "validate-~A" name)
              (:img :src "/appimg/q-icon.png")))))))







#|

(defun build-validation-array (validation-list)
  (if validation-list
      (json:encode-json-to-string
       (loop for (element type required) in validation-list
          collect `((element . ,element)
                    (type . ,type)
                    (required . ,(equalp required
                                         "required")))))))

|#