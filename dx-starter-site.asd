(asdf:defsystem :dx-starter-site
  :serial t
  :depends-on (:hunchentoot
               :closer-mop
               :cl-who
               :ht-simple-ajax
               :wfx
               :xdb2
               :cl-json
               :dx-utils)
  :components ((:file "package")
               (:file "common")               
               (:file "requests")
               
))

